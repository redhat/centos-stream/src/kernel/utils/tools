#!/bin/bash
#
# commitcmp
#

declare MYDIR=
MYDIR="$(dirname "$(which "$(basename "$0")")")"
declare MYLIB="$MYDIR"/lib

[ "$ui_loaded" ]            || source "$MYLIB"/ui.source
[ "$configmanager_loaded" ] || source "$MYLIB"/config-manager.source

declare -i background=

# Other exit codes
declare -i exit_ok=0
declare -i exit_invarg=1
declare -i exit_invpath=2	# invalid file not used yet
declare -i exit_invdir=3
declare -i exit_disfil=4	# directory name is a file
declare -i exit_nooutd=5	# directory name is empty

declare -a exitmsgary=

exitmsgary=(
""
"Invalid number of arguments."
" path does not exist."
" is an invalid directory name."
" is a file, not a directory."
"You did not provide an output directory path:$MNU -o <outdir>$OFF"
)

exitme() {
	local -i exitval=$1
	local strarg=""
	local exitmsg

	if [ "$exitval" -ne $exit_ok ] && [ "$exitval" -ne $CTLC_EXIT ]; then
		[ $# -eq 2 ] && strarg=$2
		[ ${#exitmsgary[@]} -gt "$exitval" ] \
			&& exitmsg="${exitmsgary[$exitval]}"

		echo -e "$WRN$strarg$exitmsg$OFF"

		[ "$exitval" -ne 0 ] && echo -e \
			"${INF}Type$MNU commitcmp -h$INF for help.$OFF"
	fi

	exit "$exitval"
}

# run if user hits control-c
#
control_c()
{
	echo -en "
	${WRN}Ctrl-c detected$INF
Cleaning up and exiting.$OFF
"
	exitme $CTLC_EXIT
}

declare -i optcount=0
declare uplog="upstream.log"
declare dnlog="downstream.log"
declare chlog="backport.log"
declare outdir=
declare updir=
declare upcmt=
declare dncmt=
declare subsys=
declare searchstr=
declare ssfile=

usage() {
	local usagestr

	usagestr=$(
cat <<EOF
$MNU
$(basename "$0") -ud updir -uc upcmt -dc dncmt -o outdir [-S] [sybsys]
$INF
Uses scripts ${STA}gitnice$INF and ${STA}extup$INF to create a list of
commits to backport. Commits that have already been backported will have
a leading # to effectively comment them out to processing scripts, like
git-backport.
This must be run in the downstream repo directory, e.g. the one for RHEL-9.

Creates the following files in the <outdir>.
  $STA$uplog$INF   - contains the upstream commits since <upcmt>
  $STA$dnlog$INF - contains the downstream commits since <dncmt>
  $STA$chlog$INF   - contains the upstream commits that have not yeat been
                   backported downstream.
  Optionally, if a search string was provided with the$STA -S$INF option, another
  $STA$chlog$INF file in the same directory but with the first field in the search
  string prepended to its name.
$MNU
Creates $STA$uplog$INF, $STA$dnlog$INF, $STA$chlog$INF, and
        ${STA}msg_handler-$chlog$INF in ${STA}drivers/char/ipmi/$MNU$UND
Options:$OFF$MNU
  -h$INF         - this help text$MNU
  -ud ${INF}updir  - directory with the upstream repo$MNU
  -uc ${INF}upcmt  - starting upstream commit$MNU
  -dc ${INF}dncmt  - starting downstream commit$MNU
  -o  ${INF}outdir - Directory for ouput files.
               Upstream file will be named $STA$uplog$INF
               Downstrearm file will be named $STA$dnlog$INF
               Backport file will be named $STA$chlog$MNU

For more than one of any of the following options, use a comma-separated
list with no spaces.

  -ss ${INF}sybsys - REQUIRED one or comma separated list of subsystem directories.$MNU
  -f  ${INF}file   - OPTIONAL one or comma separated list of file paths$MNU
  -S  ${INF}string - OPTIONAL one or comma separates list of search strings
	       A $STA$chlog$INF file will be created for each search string
	       with the first field of the string prepended to its name.
	       The file(s) will contain commit hashes that contain the
	       string(s).$MNU
Example:$STA
  $(basename "$0") \\\

        -ud /work/upstream/kernel \\\

        -uc v5.14 \\\

        -dc kernel-5.14.0-87.el9 \\\

        -ss drivers/char/ipmi,drivers/platform \\\

        -f  include/linux/ipmi.h,include/linux/ipmi_smi.h \\\

        -o ../backport/9.2/ipmi/ \\\

        -S ipmi_msg,kcs
$OFF
\0
EOF
)
	echo -en "$usagestr"
	exitme 0
}

chkdir() {
	local stat=0

	ui_chkdir "$1"
	stat=$?

	case $stat in
		1 ) exitme $exit_nooutd
		    ;;
		2 ) exitme $exit_disfil "$1"
		    ;;
		3 ) ;&
		4 ) exitme $exit_invdir "$1"
	esac
}

get_pathopt() {
	local path=$1
	local -n optname="$2"

	[ -e "$path" ] || exitme $exit_invpath
	optname=$(realpath "$path")
	((optcount += 2))
}

get_varopt() {
	local var="$1"
	local -n varname="$2"

	varname="${var//,/ }"
	((optcount += 2))
}

test_help() {
	local opt="$1"

	shopt -s nocasematch
	[[ $opt =~ (h|help) ]] && { usage; exitme $exit_ok; }
	shopt -u nocasematch
}

# parseops() - parse the input options
#
# Needed multicharacter optionis, but didn't want to use longopts.
#
# Inputs: $@, the whole command line
#
# Globals:
# 	optcount
# 	upcmt
# 	dncmt
# 	usagestr
# 	searchstr
# 	subsys
# 	ssfile
#
parseops() {
	local arg=
	local opt=

	for arg in "$@"; do

	    test_help "$1"

	    if [ "${arg:0:1}" == '-' ]; then
		opt="${arg:1}"

		shift
		case "$opt" in
		    ud ) get_pathopt "$1" updir
			 ;;
		    uc ) get_varopt "$1" upcmt
			 ;;
		    dc ) get_varopt "$1" dncmt
			 ;;
		    S  ) get_varopt "$1" searchstr
			 ;;
		    o  ) get_pathopt "$1" outdir
			 ;;
		    ss ) get_varopt "$1" subsys
			 ;;
		    f  ) get_varopt "$1" ssfile
			 ;;
		    *  ) echo "unrecognized option -$1"
			 echo -e "$usagestr"
			 exit 127
		esac
		shift
	    fi
	done
	((optcount += 2))
}

# compare_strings() - compare strings
#    Compare the commits from the upstream and downstream commit logs
#    to identify those the ones that have already been backported.
#    Output all commit lines to the cherp.log file, marking the ones
#    that werre already backported with a leading #
#
# GLOBALS
#	chlog - output commits file
#	uplog - upstream commits file
#	dnlog - downstream commits file
#	outdir - directory for all the commit log files
#
compare_strings() {
	local uppath="$outdir/$uplog"
	local dnpath="$outdir/$dnlog"
	local chpath="$outdir/$chlog"
	local savedifs="$IFS"
	local upstr=
	local rhstr=
	local up	# one line from the upstream log
	local tmp
	local b_match=false
	local j		# upstr index
	local k		# rhstr index

	# Instead of reading from the file, which limmits our debug
	# capabilites, create an array out of the lines in the file.
	# Set IFS to newline and then restore it after createing the
	# string arrays.
	#
	IFS=$'\n'
	upstr=($(< "$uppath"))
	rhstr=($(< "$dnpath"))
	IFS="$savedifs"

	# Zero the backport log file.
	:> "$chpath"

	for ((j = 0; j < ${#upstr[@]}; ++j)); do
		up=${upstr[j]}
		upcmt="${up:0:9}"

		for ((k = 0; k < ${#rhstr[@]}; ++k)); do
			tmp=$(echo "${rhstr[k]}" | cut -d' ' -f2)
			rhcmt="${tmp:0:9}"

			[[ "$rhcmt" == "$upcmt" ]] && {
				b_match=true
				break
			}
		done

		$b_match && echo "# $up" >> "$chpath" || echo "$up" >> "$chpath"
		$b_match && echo "# $up" || echo "$up"
		b_match=false
	done
}

set_cfg_item() {
	cfg_write_key "$1" "$2"
}

get_cfg_item() {
	cfg_read_key "$1"
}

set_colors() {
	local ans
	while :; do
		ui_use_colors
		background=$terminal_background
		set_cfg_item background $terminal_background
		echo -en "${INF}You chose "

		case $terminal_background in
			1) echo -e "${STA}light$INF background$OFF\n";;
			2) echo -e "${STA}dark$INF background$OFF\n";;
		esac

		loop_yn_ro "Is that what you want? (y/n) : " && return
	done
}

init_env() {
	b_color=true	# from lib/ui.source

	MYDATA=$(realpath ./.data)
	[ -d "$MYDATA" ] || mkdir -p "$MYDATA"

	configfile="$MYDATA/mkbackportlog.conf"
	cfgtemplate="$MYLIB/mkbackportlog.conf"
	[ -f "$configfile" ] || cp "$cfgtemplate" "$configfile"

	cfg_set_template "$cfgtemplate"
	cfg_set_configfile "$configfile"
	cfg_init

	background=$(get_cfg_item background)

	# If color hasn't already been set up, then query the user for terminal
	# background color and init the color engine.
	# Else just init the terminal_background variable from the config global.
	#
	if [ -z "$background" ] || (( background < 1 || background > 2 )); then
		set_colors
	else
		terminal_background=$background
		ui_set_colors
	fi

	set_cfg_item background "$background"
}

check_log() {
	log="$1"

	echo -e "${INF}Create : $STA$log$OFF"

	[ -f "$log" ] && {
		echo -en "$WRN$log exists.$OFF "
		echo -e "${INF}It will be overwritten if you continue.$OFF"
	}
}

get_mainbranch() {
	local line=
	local pline=
	local ary=()

	while read -r line; do
		if [[ $line == *"remote = origin"* ]]; then
			break
		else
			pline="$line"
		fi
	done < .git/config

	ui_strtok "$pline" '"' ary
	echo "${ary[1]}"
}

seek_str() {
	local prepend=
	local outfile=
	local commit
	local str

	[ -d "$updir" ] && { cd "$updir" || exitme $exit_invdir; }

	for str in $searchstr; do
		prepend=$(echo "$str" | cut -d' ' -f1)
		outfile="$outdir/$prepend-$chlog"

		echo -e "\n${STA}Creating $outfile$INF"
		: > "$outfile"
		while read -r line; do
			[ "${line:0:1}" == "#" ] && continue
			commit=$(echo "$line" | cut -d' ' -f1)
			git show "$commit" | grep -q -m1 "$str" || continue
			echo "$line" | tee -a "$outfile"
		done < "$outdir/$chlog"
	done
	cd - > /dev/null 2>&1 || exitme $exit_invdir
}

main() {
	local mainbranch

	init_env

        # Trap for control-c
        trap control_c SIGINT

	[ -f .git/config ] || {
		echo -e "${WRN}Not a repo directory!"
		echo -e "${INF}You must be in a repo directory."
		exit 1
	}
	mainbranch=$(get_mainbranch)

	parseops "$@"

	[ -n "$outdir" ] || exitme $exit_nooutd
	[ "$optcount" -lt 8 ] && exitme $exit_invarg

	shift "$optcount"
echo "outdir: $outdir"

	echo

	check_log "$outdir/$uplog"
	check_log "$outdir/$dnlog"
	check_log "$outdir/$chlog"

	echo -e "${INF}Upstream repo     : $STA$updir origin/master $OFF"
	echo -e "${INF}Downstream branch : $STA$mainbranch$INF"
	[ -n "$subsys" ] && echo -e "${INF}subsys: $STA$subsys$OFF"
	[ -n "$ssfile" ] && echo -e "${INF}ssfile: $STA$ssfile$OFF"

	echo
	echo -n "Press any key to proceed or CTRL-C to exit..."
	read -r -n1

	echo -e "$STA"
	[ -d "$updir" ] && { cd "$updir" || exitme $exit_invdir; }
	pwd
	echo -e "${STA}Creating $outdir/$uplog$INF"
	gitnice -r -c -d --no-merges "$upcmt..HEAD" $subsys $ssfile 2>&1 | tee "$outdir/$uplog"
	echo

	echo -e "$STA"
	cd - || exitme $exit_invdir
	echo -e "${STA}Creating $outdir/$dnlog$INF"
	extup -r -n -u "$updir" "$dncmt..HEAD" $subsys $ssfile 2>&1 | tee "$outdir/$dnlog"

	echo
	echo -e "${STA}Creating $outdir/$chlog$INF"
	compare_strings

	[ -n "$searchstr" ] && seek_str
	echo -e "$OFF"

	exitme $exit_ok
}

main "$@"

exitme $exit_ok

