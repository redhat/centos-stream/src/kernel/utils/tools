#!/usr/bin/bash
# SPDX-License-Identifier: GPL-2.0

# This script adds a "JIRA:https://issues.redhat.com/browse/<issue>" line
# to each commit in a branch.  It takes two arguments; the bugzilla number and the
# target branch.
#
# This script requires numpatches from the kernel-tools tree, and has MINIMAL error
# checking.
#
# For example, to insert a Jira URL for RHEL-1618 on all patches in the current branch
# that targets 8.2,
#
#	./insert_jira_URL RHEL-1618 8.2

display_help() {
  echo ""
  echo " Usage: ${0##*/} [-h] <jira_issue> <target_branch>"
  echo ""
  echo " This script adds a \"JIRA: https://issues.redhat.com/browse/<jira_issue>\" line"
  echo " to the git header of all patches in the current branch that targets the <target_branch>."
  echo ""
  echo " Note: <jira_issue> must contain the the word \"RHEL-\""
  echo ""
  echo " Examples:"
  echo ""
  echo "   ${0##*/} RHEL-1825 main"
  echo "   ${0##*/} RHEL-1946 8.5"
  echo "   ${0##*/} RHEL-1492 test_main"
  echo "   ${0##*/} -h"
  echo ""
  exit 0
}

short_help() {
  echo " ${0##*/}: Specify a Jira issue and target branch" >&2
  echo " Try \"${0##*/} -h\" for more information" >&2
  exit 1
}

if [ $# -eq 0 ]; then
	short_help
fi

BRANCH="$(git rev-parse --abbrev-ref HEAD)"
if [ -z "${BRANCH}" ]; then
	echo " $PWD is not a git repository" >&2
	exit 1
fi

JIRAID="$1"

if [[ A"$JIRAID" == A"-h" ]]; then
	display_help
fi

if [[ ! Z"$JIRAID" == Z"RHEL-"* ]]; then
	echo " Invalid argument: $1" >&2
	short_help
fi

if [ $# -ne 2 ]; then
	echo " Missing argument: must provide target_branch" >&2
	short_help
fi

TARGET=$2

if [[ A"$TARGET" == A"-h" ]]; then
	display_help
fi

DIRNAME=$(dirname $0)
NUMPATCHES=$(${DIRNAME}/numpatches "$TARGET")

isanumber='^[0-9]+$'
if ! [[ $NUMPATCHES =~ $isanumber ]] ; then
	echo " Invalid argument: $2" >&2
	short_help
fi

[ $NUMPATCHES -eq 0 ] && echo "No patches found.  Did you forget to commit?" && exit 1

echo "Found $NUMPATCHES patches for branch $BRANCH"
echo -n "Continue? (y/[n]): "
read line
case "$line" in
    y|Y)
		echo "Processing $NUMPATCHES patches"
		git filter-branch -f --msg-filter "sed -e '3i\JIRA: https://issues.redhat.com/browse/'${JIRAID}'\n'" HEAD~${NUMPATCHES}..HEAD >& /dev/null
		echo "Done!"
      ;;
    *)
		echo "Aborted!"
      ;;
esac

